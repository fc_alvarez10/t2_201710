package model.data_structures;

public class NodoDoble<T> {

	private T nodo;

	private NodoDoble<T> siguiente;

	private NodoDoble<T> anterior;


	public NodoDoble(T pNodo){
		nodo=pNodo;
		siguiente = null;
		anterior = null;
	}
	
	/*
	 * @return Nodo
	 */
	public T darNodo(){
		return nodo;
	}
	
	/*
	 * retorna el siguiente 
	 */
	public NodoDoble<T> darSiguiente(){
		return siguiente;
	}
	
	/*
	 * retorna el anterior
	 */
	public NodoDoble<T> darAnterior(){
		return anterior;
	}
	
	/*
	 * cambia el nodo  siguiente 
	 */
	public void cambiarSiguiente(NodoDoble<T> pSiguiente){
		siguiente=pSiguiente;
	}
	
	/*
	 * cambia el nodo anterior
	 */
	public void cambiarAnterior(NodoDoble<T> pAnterior){
		anterior=pAnterior;
	}

}
