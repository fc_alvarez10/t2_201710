package model.logic;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

import java.io.*;
import java.util.Iterator;

import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;


	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		// TODO Auto-generated method stub

		BufferedReader br = null;
		try {
			br =new BufferedReader(new FileReader(archivoPeliculas));
			misPeliculas = new ListaEncadenada<VOPelicula>();
			peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();
			String line = br.readLine();
			line = br.readLine();
			while (line != null) {
				String[] separacion = line.split(",");
				if (separacion.length != 3) 
					separacion = line.split("\"");
				VOPelicula nueva = new VOPelicula();
				int agno = 0;
				if (separacion[1].lastIndexOf("(") < 0)
					nueva.setTitulo(separacion[1]);
				else {
					nueva.setTitulo(separacion[1].substring(0,separacion[1].lastIndexOf("(")));
					agno = Integer.parseInt(separacion[1].substring(separacion[1].lastIndexOf("(")+1,separacion[1].lastIndexOf("(")+5));
					nueva.setAgnoPublicacion(agno); }
				if (separacion[2].startsWith("(") == false) {
					String[] gen = separacion[2].split("|");
					ListaEncadenada<String> generos = new ListaEncadenada<String>();
					for (int i = 0; i < gen.length; i++){
						generos.agregarElementoFinal(gen[i]);
					}
					nueva.setGenerosAsociados(generos);
				}
				misPeliculas.agregarElementoFinal(nueva);
				//------------------------------
				if(peliculasAgno.darNumeroElementos()==0){
					for(int i=1950;i<=2016;i++){
						VOAgnoPelicula nuevo=new VOAgnoPelicula();
						nuevo.setAgno(i);
						peliculasAgno.agregarElementoFinal(nuevo);
					}
				}
				if (agno != 0){
					Iterator<VOAgnoPelicula> iter=peliculasAgno.iterator();
					boolean esc =false; 

					while(iter.hasNext() && !esc){

						VOAgnoPelicula act=iter.next();
						if(act.getAgno()==agno){
							if(act.getPeliculas()!=null){
								ILista<VOPelicula> listaAct = act.getPeliculas();
								listaAct.agregarElementoFinal(nueva);
								act.setPeliculas(listaAct);
							}else{
								ListaDobleEncadenada<VOPelicula> listaAct = new ListaDobleEncadenada<>();
								listaAct.agregarElementoFinal(nueva);
								act.setPeliculas(listaAct);
							}

							esc=true;
						}
					}
				}
				line = br.readLine();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null!=br) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	public void cargarArchivoRatings(String archivoRatings){

		BufferedReader br=null;
		try{

			br =new BufferedReader(new FileReader(archivoRatings));
			String line=br.readLine();
			line=br.readLine();
			while(line!=null){
				String[] separacion = line.split(",");
				int userId=Integer.parseInt(separacion[0]);
				int movieId=Integer.parseInt(separacion[1]);
				double rating=Double.parseDouble(separacion[2]);
				int timestamp=Integer.parseInt(separacion[3]);

				line=br.readLine();
			}

		}catch (Exception e){
			e.printStackTrace();
		} finally {
			if (null!=br) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}


	}
	
	public void cargarArchivoTags(String archivoTags){
		BufferedReader br=null;
		try{

			br =new BufferedReader(new FileReader(archivoTags));
			String line=br.readLine();
			line=br.readLine();
			while(line!=null){
				String[] separacion = line.split(",");
				int userId=Integer.parseInt(separacion[0]);
				int movieId=Integer.parseInt(separacion[1]);
				String tag=separacion[2];
				int timestamp=Integer.parseInt(separacion[3]);

				line=br.readLine();
			}

		}catch (Exception e){
			e.printStackTrace();
		} finally {
			if (null!=br) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}




	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {
		// TODO Auto-generated method stub
		Iterator<VOPelicula> iter = misPeliculas.iterator();
		ListaEncadenada<VOPelicula> peliculas = new ListaEncadenada<VOPelicula>();
		while(iter.hasNext()){
			VOPelicula act = iter.next();
			if (act.getTitulo().contains(busqueda)) peliculas.agregarElementoFinal(act);

		}
		return peliculas;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {
		// TODO Auto-generated method stub
		return peliculasAgno.darElemento(agno-1949).getPeliculas();

	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		// TODO Auto-generated method stub
		peliculasAgno.avanzarSiguientePosicion();
		return peliculasAgno.darElementoPosicionActual();
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		// TODO Auto-generated method stub
		peliculasAgno.retrocederPosicionAnterior();
		return peliculasAgno.darElementoPosicionActual();
	}

}
