package model.data_structures;

public class NodoSencillo<T> {

	private T nodo;
	
	private NodoSencillo<T> siguiente;
	
	/*
	 * Constructor
	 */
	public NodoSencillo(T pNodo){
		nodo = pNodo;
		siguiente = null;
	}
	
	/*
	 * @return Nodo
	 */
	public T darNodo(){
		return nodo;
	}
	/*
	 * @return Retorna su siguiente
	 */
	public NodoSencillo<T> darSiguiente(){
		return siguiente;
	}
	
	/*
	 * Cambia el siguiente
	 */
	public void cambiarSiguiente(NodoSencillo<T> sigue)
	{
		siguiente = sigue;
	}
	
}