package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private NodoDoble<T> primero;
	private NodoDoble<T> actual;
	private NodoDoble<T> ultimo;
	private int size;


	@Override
	public Iterator<T> iterator() {

		actual = primero;
		class iterador<T> implements Iterator{

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return actual.darSiguiente() != null;
			}

			@Override
			public Object next() {
				// TODO Auto-generated method stub
				actual = actual.darSiguiente();
				return actual.darAnterior().darNodo();
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
			}
		}
		Iterator<T> iter = new iterador();
		return iter;

	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		NodoDoble<T> nuevo = new NodoDoble<T>(elem);
		if (size ==0 ){	primero = nuevo;
		actual = primero;}
		else {
			ultimo.cambiarSiguiente(nuevo);
			nuevo.cambiarAnterior(ultimo);}
		ultimo = nuevo;
		size++;
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		int cont = 0;
		actual = primero;
		while (cont < pos){
			actual = actual.darSiguiente();	
			cont++;
		}
		return actual.darNodo();
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return actual.darNodo();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if (actual.darSiguiente() == null) return false;
		actual = actual.darSiguiente();
		return true;

	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if (actual.darAnterior() == null) return false;
		actual = actual.darAnterior();
		return true;
	}

}
